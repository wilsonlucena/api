<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatabaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('email');
            $table->string('documento');
            $table->string('telefone');
        });

        Schema::create('contas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clientes_id')->unsigned();
            $table->decimal('saldo', 8,2)->default('0.00')->nullable();
            $table->foreign('clientes_id')
                ->references('id')->on('clientes')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        Schema::create('historicos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('conta_id')->unsigned();
            $table->string('produto')->nullable();
            $table->decimal('saldo_anterior', 8,2)->nullable();
            $table->decimal('credito', 8,2)->nullable();
            $table->decimal('debito', 8,2)->nullable();
            $table->decimal('saldo', 8,2)->nullable();
            $table->timestamp('data')->useCurrent();
            $table->foreign('conta_id')->references('id')->on('contas')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historicos');
        Schema::dropIfExists('contas');
        Schema::dropIfExists('clientes');
    }
}
