<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->group(function () {
    Route::get('dashboard', function () {
        return response()->json(['data' => 'Test Data']);
    });
});

Route::group(['middleware' => 'api','prefix' => 'auth'], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});

Route::group(['middleware' => 'api','prefix'=>'/'], function ($router) {

    Route::get('usuarios', 'UsuarioController@index');
    Route::get('/usuario/{id}', 'UsuarioController@get');
    Route::post('/usuario', 'UsuarioController@register');
    Route::put('/usuario/{id}', 'UsuarioController@update');
    Route::delete('/usuario/{id}', 'UsuarioController@delete');

});

Route::group(['middleware' => 'api','prefix'=>'/'], function ($router) {

    Route::get('clientes', 'ClienteController@index');
    Route::get('cliente/{id}', 'ClienteController@get');
    Route::post('cliente', 'ClienteController@store');
    Route::put('cliente/{id}', 'ClienteController@update');
    Route::delete('cliente/{id}', 'ClienteController@destroy');

});

Route::group(['middleware' => 'api','prefix'=>'/'], function ($router) {

    Route::get('extrato/{cliente}', 'ContaController@extrato');
    Route::get('cliente/{id}', 'ContaController@get');
    Route::post('debito/{cliente}', 'ContaController@debito');
    Route::post('credito/{cliente}', 'ContaController@credito');
    Route::post('registra/compra/{cliente}', 'ContaController@compra');

});


