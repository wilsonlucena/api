<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Historico extends Model
{
    protected $fillable = ['conta_id', 'produto','credito', 'debito', 'saldo','saldo_anterior', 'data'];

    public function contas()
    {
        return $this->belongsTo(Conta::class);
    }

    public $timestamps = false;


}
