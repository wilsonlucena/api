<?php

namespace App\Entities;

use App\Entities\Historico;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Conta.
 *
 * @package namespace App\Entities;
 */
class Conta extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['clientes_id', 'saldo'];

    public function cliente():HasOne
    {
        return $this->hasOne(Cliente::class);
    }

    public function historicos()
    {
        return $this->hasMany(Historico::class);
    }

    public $timestamps = false;

}
