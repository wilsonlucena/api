<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\Cliente;
use App\Validators\ClienteValidator;

/**
 * class ClienteRepository.
 *
 * @package namespace App\Repositories;
 */
class ClienteRepository extends  BaseRepository implements RepositoryInterface
{
    protected $fieldSearchable = [
         'nome', 'email', 'telefone', 'documento'
        ];
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Cliente::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
