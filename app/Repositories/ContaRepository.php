<?php

namespace App\Repositories;

use App\Entities\Conta;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Interface ContaRepository.
 *
 * @package namespace App\Repositories;
 */
class ContaRepository extends BaseRepository implements RepositoryInterface
{

    protected $fieldSearchable = ['clientes_id', 'saldo'];
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Conta::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
