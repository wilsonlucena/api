<?php
/**
 * Created by PhpStorm.
 * User: wilson
 * Date: 25/12/2018
 * Time: 19:33
 */

namespace App\Http;


class Util
{
    public static function removeMask($valor){
        $valor = trim($valor);
        $valor = str_replace(".", "", $valor);
        $valor = str_replace(",", "", $valor);
        $valor = str_replace("-", "", $valor);
        $valor = str_replace("/", "", $valor);

        return $valor;
    }
}