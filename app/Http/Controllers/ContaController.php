<?php
/**
 * Created by PhpStorm.
 * User: wilson
 * Date: 13/12/2018
 * Time: 00:15
 */

namespace App\Http\Controllers;


use App\Entities\Cliente;
use App\Entities\Conta;
use App\Entities\Historico;
use App\Repositories\ContaRepository;
use Symfony\Component\HttpFoundation\Request;


class ContaController extends AbstractController
{

    public function __construct(ContaRepository $repository)
    {
        parent::__construct($repository);
    }

    /**
     * @SWG\Get(
     *   path="/cliente/{id}",
     *   summary="Ver informações da conta do cliente",
     *
     *   @SWG\Response(response=200, description="Success in operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function get($id)
    {
        $cliente =  Cliente::find($id);

        $conta = $this->repository->with('historicos')->findByField('clientes_id',$id)->first();

        return response()->json(['cliente'=>$cliente, 'conta'=>$conta]);
    }
    /**
     * function created to add credit to users account
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *   path="/credito/{cliente}",
     *   summary="Adiciona creditos na conta do cliente",
     * @SWG\Parameter(
     *     name="valor",
     *     in="path",
     *     description="Valor",
     *     required=true,
     *     type="number"
     *  ),
     *   @SWG\Response(response=200, description="Success in operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function credito(Request $request, $id)
    {
        $cliente = Cliente::find($id);

        $conta = Conta::where('clientes_id', $cliente->id)->first();

        $saldoAnterior = $conta->saldo;
        $conta->saldo = ($conta->saldo + $request->valor);
        $conta->save();

        $data = [
            'conta_id'=> $conta->id,
            'saldo_anterior'=> $saldoAnterior,
            'credito'=> $request->valor,
            'saldo'=> $conta->saldo,
            'produto'=> 'CRÉDITO',
            'data'=> new \DateTime('now')

        ];

        Historico::create($data);

        return response()->json(['message'=>'Creditos atualizados com sucesso '], 200);

    }

    /**
     * function created to add credit to users account
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *   path="/debito/{cliente}",
     *   summary="Registra debitos",
     * @SWG\Parameter(
     *     name="valor",
     *     in="path",
     *     description="Valor",
     *     required=true,
     *     type="number"
     *  ),
     *   @SWG\Response(response=200, description="Success in operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function debito(Request $request, $id)
    {
        $cliente = Cliente::find($id);

        $conta = Conta::where('clientes_id', $cliente->id)->first();

        $saldoAnterior = $conta->saldo;
        $conta->saldo = ($conta->saldo - $request->valor);



        $conta->save();

        $data = [
            'conta_id'=> $conta->id,
            'saldo_anterior'=> $saldoAnterior,
            'debito'=> $request->valor,
            'saldo'=> $conta->saldo,
            'produto' => 'DEBITO'
        ];

        Historico::create($data);

        return response()->json(['message'=>'Débito registrado com sucesso '], 200);

    }

    /**
     * function created to add credit to users account
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *   path="/registra/compra/{cliente}",
     *   summary="Registra compras e debita da conta do cliente",
     * @SWG\Parameter(
     *     name="valor",
     *     in="path",
     *     description="Valor",
     *     required=true,
     *     type="number"
     *  ),
     * @SWG\Parameter(
     *     name="produto",
     *     in="path",
     *     description="Descrição do produto (Credito)",
     *     required=true,
     *     type="string"
     *  ),
     *   @SWG\Response(response=200, description="Success in operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function compra(Request $request, $id)
    {
        $cliente = Cliente::find($id);

        $conta = Conta::where('clientes_id', $cliente->id)->first();

        $saldoAnterior = $conta->saldo;
        $conta->saldo = ($conta->saldo - $request->valor);



        $conta->save();

        $data = [
            'conta_id'=> $conta->id,
            'saldo_anterior'=> $saldoAnterior,
            'debito'=> $request->valor,
            'saldo'=> $conta->saldo,
            'produto' => $request->produto
        ];

        Historico::create($data);

        return response()->json(['message'=>'Consumo registrado com sucesso '], 200);

    }


    /**
     * @SWG\Get(
     *   path="/extrato/{cliente}",
     *   summary="Ver extrato da conta do cliente",
     *
     *   @SWG\Response(response=200, description="Success in operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function extrato($cliente)
    {
        $data =  Cliente::find($cliente);


        $extrato = $this->repository->with('historicos')->findByField('clientes_id',$cliente)->first();
        $extrato['nome'] = $data->nome;
        $extrato['documento'] = $data->documento;

        return response()->json(['extrato'=>$extrato]);
    }


}