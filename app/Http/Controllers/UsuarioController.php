<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use JWTAuth;

class UsuarioController extends Controller
{

    /**
     * @SWG\Get(
     *   path="/usuarios",
     *   summary="Usuários cadastrados no sistema",
     *
     *   @SWG\Response(response=200, description="Success in operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function index()
    {
        $clientes = User::all();
        return response()->json($clientes);
    }


    /**
     * @SWG\Get(
     *   path="/usuario/{id}",
     *   summary="Atualiza usuario",

     *
     *   @SWG\Response(response=200, description="Success in operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function get($id)
    {
        $cliente = User::findOrFail($id);
        return response()->json($cliente);
    }

    /**
     * @SWG\Post(
     *   path="/usuario",
     *   summary="Cadastrar usuario",
     *  @SWG\Parameter(
     *     name="name",
     *     in="path",
     *     description="Nome",
     *     required=true,
     *     type="string"
     *  ),
     *  @SWG\Parameter(
     *     name="email",
     *     in="path",
     *     description="Email",
     *     required=true,
     *     type="string"
     *  ),
     *  @SWG\Parameter(
     *     name="password",
     *     in="path",
     *     description="Senha",
     *     required=true,
     *     type="string"
     *  ),
     *
     *   @SWG\Response(response=200, description="Success in operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function register(Request $request)
    {
        $rules = $request->all();
        $user = new User();
        $user->name = $rules['name'];
        $user->email = $rules['email'];
        $user->password = bcrypt($rules['password']);
        $user::first();
        $token = JWTAuth::fromUser($user);
        $user->save();

        return response()->json([
            'message'=>'Registro inserido com sucesso',
            'token'=> compact('token')

        ], 200);

    }
    /**
     * @SWG\Put(
     *   path="/usuario/{id}",
     *   summary="Atualizar usuario",
     *
     *   @SWG\Response(response=200, description="Success in operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function update(Request $request, $id)
    {
        $rules = $request->all();
        $user = User::findOrFail($id);
        $user->name = $rules['name'];
        $user->email = $rules['email'];
        $user->save();
        return response()->json(['message'=>'Registro atualizado com sucesso'], 200);
    }
    /**
     * @SWG\Delete(
     *   path="/usuario/{id}",
     *   summary="Delete usuario",
     *
     *   @SWG\Response(response=200, description="Success in operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function delete($id)
    {
        $cliente = User::findOrFail($id);
        $cliente->delete();
        return response()->json(['message'=>'Registro excluido com sucesso'], 200);
    }
}
