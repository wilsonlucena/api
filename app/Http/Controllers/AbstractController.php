<?php
/**
 * Created by PhpStorm.
 * User: wilson
 * Date: 17/12/2018
 * Time: 21:53
 */

namespace App\Http\Controllers;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
/**
 * @SWG\Swagger(
 *   basePath="/api",
 *   @SWG\Info(
 *     title="[API MeFidelizei] -- mefidelizei.com/api/{url}",
 *     version="1.0.0"
 *   )
 * )
 */
class AbstractController extends Controller
{
    protected $repository;


    public function __construct($repository)
    {
        $this->repository = $repository;
    }


    public function index(Request $request)
    {
        $entity = $this->repository->all();

        return response()->json($entity->toArray(), 200);

    }

    public function get($id)
    {
        $cliente = $this->repository->find($id);
        return response()->json($cliente->toArray());
    }

    /**
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        try{
            $this->repository->create($input);

        }catch (\Exception $e){
            dd($e->getMessage());
        }

        return response()->json(['message'=>'Registro cadastrado com sucesso '], 200);
    }

    /**
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $entity = $this->repository->findWithoutFail($id);

        if (empty($controller)) {
            return $this->sendError('NÃ£o existe');
        }

        return response()->json($entity->toArray());
    }

    /**
     * @param int     $id
     * @param Request $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $atributtes = $request->all();

        $controller = $this->repository->find($id);

        if (empty($controller)) {
            return $this->sendError('NÃ£o Existe');
        }

        try{
            $this->repository->update($atributtes, $id);

        }catch (\Exception $e){
            dd($e->getMessage());
        }

        return response()->json(['message'=>'Registro realizado com sucesso '], 200);
    }

    /**
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $entity = $this->repository->find($id);

        if (empty($entity)) {
            return $this->sendError('Nao Existe');
        }

        $entity->delete();

        return response()->json(['message'=>'Excluido com sucesso'], 200);

    }
}